#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

size_t round_up(size_t size, size_t block) {
    return ((size + (block - 1)) / block) * block;
}

int yesDialog(const char * msg) {
    char * answer = NULL;
    size_t size = 0;

    if (isatty(STDIN_FILENO)) {
        printf("\nWARNING!\n========\n");
        printf("%s\n\nAre you sure? (Type uppercase yes): ", msg);
        fflush(stdout);
        if(!getline(&answer, &size, stdin)) {
            free(answer);
            return 0;
        }
        int r = strcmp(answer, "YES\n");
        free(answer);
        return r == 0 ? 1 : 0;
    }
    return 1;
}

int device_open(const char * name, int flags) {
    int fd = open(name, flags);
    if (fd < 0)
        printf("Cannot open device %s%s.", name, (flags & O_ACCMODE) != O_RDONLY ? " for write" : "");
    return fd;
}