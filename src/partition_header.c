#include "partition_header.h"
#include "af.h"

int findEmpty(struct PHDR * header) {
    for (int i = 0; i < LUKS_NUMKEYS; ++i) {
        if (header->keyblock->active == LUKS_KEY_ENABLED)
            return i;
    }
    return -1;
}

int keyslotArea(const struct PHDR * hdr, int keyslot, uint64_t * offset, uint64_t * length) {
    if(keyslot >= LUKS_NUMKEYS || keyslot < 0)
        return 0;

    *offset = (uint64_t)hdr->keyblock[keyslot].keyMaterialOffset * LUKS_SECTOR_SIZE;
    *length = AF_split_sectors(hdr->keyBytes, LUKS_STRIPES) * LUKS_SECTOR_SIZE;
    return 1;
}