#ifndef FITLUKS_RANDOM_H
#define FITLUKS_RANDOM_H

#include <fcntl.h>
#include <unistd.h>

#define URANDOM_DEVICE "/dev/urandom"
#define RANDOM_DEVICE "/dev/random"

///https://sites.uclouvain.be/SystInfo/usr/include/asm-generic/fcntl.h.html
int writeRandom(char * array, size_t size);
int writeUrandom(char * array, size_t size);

#endif //FITLUKS_RANDOM_H
