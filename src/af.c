#include <unistd.h>
#include "partition_header.h"

size_t AF_split_sectors(size_t blocksize, unsigned int blocknumbers) {
    size_t af_size = blocksize * blocknumbers; /// data material * stripes
    af_size = (af_size + (LUKS_SECTOR_SIZE - 1)) / LUKS_SECTOR_SIZE; /// round up to sector
    return af_size;
}