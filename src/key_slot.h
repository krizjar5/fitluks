#ifndef FITLUKS_KEY_SLOT_H
#define FITLUKS_KEY_SLOT_H

#include <stdint.h>

#define LUKS_SALTSIZE     32
#define LUKS_KEY_DISABLED 0x0000DEAD
#define LUKS_KEY_ENABLED  0x00AC71F3
#define LUKS_STRIPES      4000

struct key_slot {
    uint32_t active;
    uint32_t passwordIterations;
    char     passwordSalt[LUKS_SALTSIZE];
    uint32_t keyMaterialOffset;
    uint32_t stripes;
};

#endif //FITLUKS_KEY_SLOT_H
