#ifndef FITLUKS_LUKS_H
#define FITLUKS_LUKS_H

#include "volumeKey.h"

extern int opt_dump_master_key;
extern int opt_random;
extern int opt_urandom;
extern int opt_iteration_time;

extern long opt_pbkdf_iterations;

extern char * opt_cipher;
extern char * opt_cipher_mode;
extern char * opt_hash;
extern char * opt_uuid;

int action_luksFormat(const char * name);
int action_luksAddKey(const char * name, struct volume_key * vk);
int action_luksRemoveKey(void);
int action_luksKillSlot(void);
int action_isLuks(const char * name);
int action_luksUUID(const char * name);
int action_luksDump(const char *name);

#endif //FITLUKS_LUKS_H
