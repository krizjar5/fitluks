#ifndef FITLUKS_VOLUMEKEY_H
#define FITLUKS_VOLUMEKEY_H

#include <unistd.h>

struct volume_key {
    size_t keylength;
    char key[];
};

struct volume_key * generate_key(size_t keyLength);
int reconstruct_key(struct volume_key * key);

#endif // FITLUKS_VOLUMEKEY_H
