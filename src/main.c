#include <stdio.h>
#include <unistd.h>
#include <popt.h> // sudo apt install libpopt-dev
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>

#include "LUKS.h"
#include "utils.h"

int opt_dump_master_key = 0;
int opt_random = 0;
int opt_urandom = 0;
int opt_iteration_time = 0;

long opt_pbkdf_iterations = 0;

char * opt_cipher = NULL;
char * opt_cipher_mode = NULL;
char * opt_hash = NULL;
char * opt_uuid = NULL;

const char * action_name;
static const char ** action_argv = NULL;
static int action_argc;

void print_and_exit(poptContext popt_context, int status, char * msg) {
    poptFreeContext(popt_context);
    printf("%s\n", msg);
    exit(status);
}

int main(int argc, const char * argv[]) {
    static struct poptOption popt_option[] = {
        { "cipher",                 'c',  POPT_ARG_STRING, &opt_cipher,           0, "The cipher used to encrypt the disk (see /proc/crypto)", NULL },
        { "hash",                   'h',  POPT_ARG_STRING, &opt_hash,             0, "The hash used to create the encryption key from the passphrase", NULL },
        { "dump-master-key",        '\0', POPT_ARG_NONE,   &opt_dump_master_key,  0, "Dump volume (master) key instead of keyslots info", NULL },
        { "use-random",             '\0', POPT_ARG_NONE,   &opt_random,           0, "Use /dev/random for generating volume key", NULL },
        { "use-urandom",            '\0', POPT_ARG_NONE,   &opt_urandom,          0, "Use /dev/urandom for generating volume key", NULL },
        { "uuid",                   '\0', POPT_ARG_STRING, &opt_uuid,             0, "UUID for device to use", NULL },
        { "iter-time",              'i',  POPT_ARG_INT,    &opt_iteration_time,   0, "PBKDF iteration time for LUKS (in ms)", NULL },
        { "pbkdf-force-iterations", '\0', POPT_ARG_LONG,   &opt_pbkdf_iterations, 0, "PBKDF iterations cost (forced, disables benchmark)", NULL },
        POPT_TABLEEND
    };

    poptContext popt_context = poptGetContext(NULL, argc, argv, popt_option, 0);
    poptSetOtherOptionHelp(popt_context, "\n\t[OPTION...] <action> <action-specific>");

    if (argc < 2) {
        poptPrintUsage(popt_context, stdout, 0);
        poptFreeContext(popt_context);
        return 0;
    }

    if (poptGetNextOpt(popt_context) < -1) {
        printf("Unknown error.\n");
        return 1;
    }

    if (!(action_name = poptGetArg(popt_context)))
        print_and_exit(popt_context, EXIT_FAILURE, "Argument <action> missing.");

    action_argc = 0;
    action_argv = poptGetArgs(popt_context);
    while(action_argv[action_argc] != NULL) {
        //printf("%s\n", action_argv[action_argc]);
        action_argc++;
    }

    /** checking for nonsense **/
    if (action_argc != 1)
        print_and_exit(popt_context, EXIT_FAILURE, "Missing <device>");
    if (opt_random && opt_urandom)
        print_and_exit(popt_context, EXIT_FAILURE, "Only one of --use-[u]random options is allowed.");
    if ((opt_random || opt_urandom) && strcmp(action_name, "luksFormat"))
        print_and_exit(popt_context, EXIT_FAILURE, "Option --use-[u]random is allowed only for luksFormat.");
    if (opt_dump_master_key && strcmp(action_name, "luksFormat"))
        print_and_exit(popt_context, EXIT_FAILURE, "Option --dump-master-key is allowed only for luksFormat.");
    if (opt_uuid && strcmp(action_name, "luksFormat") && strcmp(action_name, "luksUUID"))
        print_and_exit(popt_context, EXIT_FAILURE, "Option --uuid is allowed only for luksFormat and luksUUID.");
    if (opt_pbkdf_iterations && opt_iteration_time)
        print_and_exit(popt_context, EXIT_FAILURE, "PBKDF forced iterations cannot be combined with iteration time option.");

    if (!strcmp(action_name, "luksDump"))
        action_luksDump(action_argv[0]);
    else if (!strcmp(action_name, "isLuks"))
        action_isLuks(action_argv[0]);
    else if (!strcmp(action_name, "luksUUID"))
        action_luksUUID(action_argv[0]);
    else if (!strcmp(action_name, "luksFormat"))
        action_luksFormat(action_argv[0]);
    else
        /**
         * "open"
         * "close"
         * "luksAddKey"
         * "luksRemoveKey"
         * "luksKillSlot"
         * "luksHeaderBackup"
         * "luksHeaderRestore"
         */
        {
        printf("Unknown action.\n");
        poptFreeContext(popt_context);
        return 1;
    }

    poptFreeContext(popt_context);
    return 0;
}