#ifndef FITLUKS_AF_H
#define FITLUKS_AF_H

#include <unistd.h>

size_t AF_split_sectors(size_t blocksize, unsigned int blocknumbers);

#endif //FITLUKS_AF_H
