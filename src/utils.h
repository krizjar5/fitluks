#ifndef FITLUKS_UTILS_H
#define FITLUKS_UTILS_H

#include <unistd.h>

size_t round_up(size_t size, size_t block);
int yesDialog(const char * msg);
int device_open(const char * name, int flags);

#endif //FITLUKS_UTILS_H
