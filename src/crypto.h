#ifndef FITLUKS_CRYPTO_H
#define FITLUKS_CRYPTO_H

#include <string.h>

void memzero(void * s, size_t n);

#endif //FITLUKS_CRYPTO_H
