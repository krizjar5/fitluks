#include <string.h>
#include <stdint.h>

void memzero(void * s, size_t n) {
#ifdef HAVE_EXPLICIT_BZERO
    explicit_bzero(s, n);
#else
    volatile uint8_t *p = (volatile uint8_t *)s;

    while (n--)
        *p++ = 0;
#endif /* HAVE_EXPLICIT_BZERO */
}
