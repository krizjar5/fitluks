#ifndef FITLUKS_PARTITION_HEADER_H
#define FITLUKS_PARTITION_HEADER_H

#include <stdio.h>
#include <stdint.h>
#include "key_slot.h"

#define LUKS_MAGIC_L            6
#define LUKS_MAGIC              {'L', 'U', 'K', 'S', (char)0xBA, (char)0xBE}
#define LUKS_CIPHERNAME_L       32
#define LUKS_CIPHERMODE_N       32
#define LUKS_HASHSPEC_L         32
#define UUID_STRING_L           40
#define LUKS_DIGESTSIZE         20
#define LUKS_SALTSIZE           32
#define LUKS_MKD_ITERATIONS_MIN 1000

#define DEFAULT_DISK_ALIGNMENT 2097152 /** 2 MiB **/
#define LUKS_ALIGN_KEYSLOTS    4096
#define LUKS_SECTOR_SIZE       512

#define LUKS_NUMKEYS 8
#define KEY_SIZE 32

struct PHDR {
    char            magic[LUKS_MAGIC_L];
    uint16_t        version;
    char            cipherName[LUKS_CIPHERNAME_L];
    char            cipherMode[LUKS_CIPHERMODE_N];
    char            hashSpec[LUKS_HASHSPEC_L];
    uint32_t        payloadOffset;
    uint32_t        keyBytes;
    char            mkDigest[LUKS_DIGESTSIZE];
    char            mkDigestSalt[LUKS_SALTSIZE];
    uint32_t        mkDigestIterations;
    char            uuid[UUID_STRING_L];
    struct key_slot keyblock[LUKS_NUMKEYS];
    char            _padding[432];
} __attribute__ ((__packed__));

int findEmpty(struct PHDR * header);
int keyslotArea(const struct PHDR * hdr, int keyslot, uint64_t * offset, uint64_t * length);

#endif //FITLUKS_PARTITION_HEADER_H
