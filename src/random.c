#include "random.h"

int writeRandom(char * array, size_t size) {
    int fileDescriptor = open(URANDOM_DEVICE, O_RDONLY);
    if (fileDescriptor == -1)
        return 0;
    ssize_t error = read(fileDescriptor, array, size);
    if (error == -1)
        return 0;
    return !close(fileDescriptor);
}

int writeUrandom(char * array, size_t size) {
    int fileDescriptor = open(RANDOM_DEVICE, O_RDONLY | O_NONBLOCK | O_CLOEXEC);
    if (fileDescriptor == -1)
        return 0;
    ssize_t error = read(fileDescriptor, array, size);
    if (error == -1)
        return 0;
    return !close(fileDescriptor);
}