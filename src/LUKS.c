#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include <uuid/uuid.h> //sudo apt-get install uuid-dev

#include "LUKS.h"
#include "partition_header.h"
#include "key_slot.h"
#include "crypto.h"
#include "af.h"
#include "utils.h"
#include "volumeKey.h"
#include "random.h"

int action_luksFormat(const char * name) {
    if (action_isLuks(name))
        printf("WARNING: Device %s already contains a 'crypto_LUKS' superblock signature.\n", name);
    struct PHDR header;
    memset(&header, '\0', sizeof(header));

    /** Check size of the device **/
    FILE * f = fopen(name, "r");
    if (!f)
        return 0;
    fseek(f, 0, SEEK_END);
    long size = ftell(f);
    if (size < sizeof(header) + (DEFAULT_DISK_ALIGNMENT / LUKS_SECTOR_SIZE)) {
        printf("Not enough space - %ld out of %ld\n", size, sizeof(header) + (DEFAULT_DISK_ALIGNMENT / LUKS_SECTOR_SIZE));
        fclose(f);
        return 0;
    }
    fclose(f);

    /** Just OK test **/
    //char * msg = "This will overwrite data on %s irrevocably.";
    //int len = snprintf(NULL, 0, msg, name);
    //if (len < 0)
    //    return 0;
    //
    //char * buff = (char*)malloc(len+1);
    //snprintf(buff, len + 1, msg, name);
    //if (!yesDialog(buff)) {
    //    free(buff);
    //    return 0;
    //}
    //free(buff);

    struct volume_key * vk = generate_key(KEY_SIZE);
    char magic[] = LUKS_MAGIC;

    if (!opt_hash)
        opt_hash = DEFAULT_LUKS1_HASH;

    if (!opt_cipher) {
        opt_cipher = DEFAULT_LUKS1_CIPHER;
        opt_cipher_mode = DEFAULT_LUKS1_CIPHERMODE;
    }

    char * res;
    if ((res = verify_crypto(opt_cipher, opt_hash))) {
        printf("%s\n", res);
        destroy_key(vk);
        return 0;
    }

    strncpy(header.magic, magic, LUKS_MAGIC_L);
    header.version = htons((uint16_t)1);
    strcpy(header.cipherName, opt_cipher);
    strcpy(header.cipherMode, opt_cipher_mode);
    strcpy(header.hashSpec, opt_hash);
    header.keyBytes = htonl(KEY_SIZE);

    if (opt_random) {
        if (!writeRandom(header.mkDigestSalt, (size_t)LUKS_SALTSIZE)) {
            destroy_key(vk);
            return 0;
        }
    }
    else {
        if (!writeUrandom(header.mkDigestSalt, (size_t)LUKS_SALTSIZE)) {
            destroy_key(vk);
            return 0;
        }
    }

    header.mkDigestIterations = htonl(LUKS_MKD_ITERATIONS_MIN);

    /** PBKDF2 **/
    if (!PKCS5_PBKDF2_HMAC_SHA1(vk->key, vk->keylength, (unsigned char*)header.mkDigestSalt, LUKS_SALTSIZE,
                                LUKS_MKD_ITERATIONS_MIN, LUKS_DIGESTSIZE, (unsigned char*)header.mkDigest)) {
        printf("PBKDF2 failed.\n");
        destroy_key(vk);
        return 0;
    }

    /** Keyslot setting **/
    size_t baseOffset = LUKS_ALIGN_KEYSLOTS / LUKS_SECTOR_SIZE;
    size_t keyMaterialSectors = AF_split_sectors(vk->keylength, LUKS_STRIPES);

    for (int i = 0; i < LUKS_NUMKEYS; i++) {
        header.keyblock[i].active = htonl(LUKS_KEY_DISABLED);
        header.keyblock[i].keyMaterialOffset = htonl(baseOffset);
        header.keyblock[i].stripes = htonl(LUKS_STRIPES);
        baseOffset = round_up(baseOffset + keyMaterialSectors, LUKS_ALIGN_KEYSLOTS / LUKS_SECTOR_SIZE);
    }

    header.payloadOffset = htonl(DEFAULT_DISK_ALIGNMENT / LUKS_SECTOR_SIZE);

    /** Generate UUID **/
    char temp_uuid[UUID_STRING_L];
    uuid_t temp;
    uuid_generate(temp);
    uuid_unparse_lower(temp, temp_uuid);
    memcpy(header.uuid, temp_uuid, UUID_STRING_L);

    /** Wipe keyslots, just to be sure**/
    uint64_t offset = 0, length = (uint64_t)header.payloadOffset * LUKS_SECTOR_SIZE;
    for (int i = 0; i < LUKS_NUMKEYS; i++) {
        if (!keyslotArea(&header, i, &offset, &length)) {
            destroy_key(vk);
            return 0;
        }
    }

    int fd = device_open(name, O_RDWR);
    if (sizeof(header) != write(fd, &header, sizeof(header))) { // valgrind: struct align error? TODO: buffer
        printf("Unknown error occurred.\n");
        destroy_key(vk);
        close(fd);
        return 0;
    }
    close(fd);

    ///call addkey

    destroy_key(vk);
    return 1;
}

int action_luksAddKey(const char * name, struct volume_key * vk) {
    if (!action_isLuks(name))
        return 0;
    struct PHDR header;
    findEmpty(&header);
    return 1;
}

int action_luksRemoveKey(void) {
    return 1;
}

int action_luksKillSlot(void) {
    return 1;
}

int action_isLuks(const char * name) {
    FILE * f = fopen(name, "r");
    if (!f)
        return 0;
    char buffer[LUKS_MAGIC_L];
    char magic[] = LUKS_MAGIC;
    if (LUKS_MAGIC_L != fread(buffer, 1, LUKS_MAGIC_L, f)) {
        fclose(f);
        return 0;
    }
    int res = !memcmp(magic, buffer, LUKS_MAGIC_L);
    fclose(f);
    return res;
}

int action_luksUUID(const char * name) {
    FILE * f = fopen(name, "r");
    if (!f)
        return 0;
    struct PHDR header;
    if (sizeof(header) != fread(&header, 1, sizeof(header), f))
        return 0;
    fclose(f);
    printf("%s\n", header.uuid);
    return 1;
}

int action_luksDump(const char *name) {
    if (!name)
        return 0;
    FILE * f = fopen(name, "r");
    if (!f || !action_isLuks(name)) {
        printf("Device %s is not a valid LUKS device.\n", name);
        return 0;
    }

    struct PHDR header;
    if (sizeof(header) != fread(&header, 1, sizeof(header), f)) { //FIXME - velikost celého payloadu
        fclose(f);
        return 0;
    }
    fclose(f);

    if (opt_dump_master_key) {
        //struct volume_key * key;
        //int res = reconstruct_key(key);
        //for (size_t i = 0; i < key->keylength; ++i) {
        //    printf("%02x%c", (unsigned char)key->key[i], i == LUKS_DIGESTSIZE - 1 ? '\n' : ' ');
        //}
        //destroy_key(key);
        //return res;
    }

    if (ntohs(header.version) != 1) {
        printf("LUKS2 is not supported yet.\n");
        return 1;
    }
    printf("LUKS header information for %s\n\n", name);
    printf("Version:       \t%" PRIu16 "\n", ntohs(header.version));
    printf("Cipher name:   \t%s\n", header.cipherName);
    printf("Cipher mode:   \t%s\n", header.cipherMode);
    printf("Hash spec:     \t%s\n", header.hashSpec);
    printf("Payload offset:\t%" PRIu32 "\n", ntohl(header.payloadOffset));
    printf("MK bits:       \t%" PRIu32 "\n", ntohl(header.keyBytes) * 8);
    printf("MK digest:     \t");
    for (int i = 0; i < LUKS_DIGESTSIZE; ++i)
        printf("%02x%c", (unsigned char)(header.mkDigest[i]), i == LUKS_DIGESTSIZE - 1 ? '\n' : ' ');
    printf("MK salt:       \t");
    for (int i = 0; i < LUKS_SALTSIZE / 2; ++i)
        printf("%02x%c", (unsigned char)(header.mkDigestSalt[i]), i == (LUKS_SALTSIZE / 2)-1 ? '\n' : ' ');
    printf("               \t");
    for (int i = LUKS_SALTSIZE / 2; i < LUKS_SALTSIZE; ++i)
        printf("%02x%c", (unsigned char)(header.mkDigestSalt[i]), i == LUKS_SALTSIZE - 1 ? '\n' : ' ');
    printf("MK iterations: \t%" PRIu32 "\n", ntohl(header.mkDigestIterations));
    printf("UUID:          \t%s\n\n", header.uuid);
    for(int i = 0; i < LUKS_NUMKEYS; i++) {
        if(header.keyblock[i].active == LUKS_KEY_ENABLED) {
            printf("Key Slot %d: ENABLED\n",i);
            printf("\tIterations:         \t%" PRIu32 "\n", ntohl(header.keyblock[i].passwordIterations));
            printf("\tSalt:               \t");
            for (int j = 0; j < LUKS_SALTSIZE/2; ++j)
                printf("%02x%c", (unsigned char)(header.keyblock[i].passwordSalt[j]), j == (LUKS_SALTSIZE/2)-1 ? '\n' : ' ');
            printf("\t                      \t");
            for (int j = LUKS_SALTSIZE / 2; j < LUKS_SALTSIZE; ++j)
                printf("%02x%c", (unsigned char)(header.keyblock[i].passwordSalt[j]), j == LUKS_SALTSIZE - 1 ? '\n' : ' ');
            printf("\tKey material offset:\t%" PRIu32 "\n",    ntohl(header.keyblock[i].keyMaterialOffset));
            printf("\tAF stripes:            \t%" PRIu32 "\n", ntohl(header.keyblock[i].stripes));
        }
        else
            printf("Key Slot %d: DISABLED\n", i);
    }
    return 1;
}