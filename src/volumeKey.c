#include <unistd.h>
#include <stdlib.h>

#include "random.h"
#include "volumeKey.h"
#include "LUKS.h"

struct volume_key * generate_key(size_t keyLength) {
    struct volume_key * vk;

    vk = (struct volume_key *)malloc(sizeof(*vk) + keyLength);
    if (!vk)
        return NULL;

    vk->keylength = keyLength;
    if (opt_urandom) {
        if (!writeUrandom(vk->key, keyLength))
            return 0;
    }
    else {
        if (!writeRandom(vk->key, keyLength))
            return 0;
    }
    return vk;
}

int reconstruct_key(struct volume_key * key) {
    
    return 1;
}